package fr.ib.tp_cuisine;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConversionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] unites = {"kg", "g", "L", "pincée", "cuillère à café", "cuillère à soupe", "tasse à café"};
		
		req.setAttribute("unites", unites);
		req.getRequestDispatcher("/WEB-INF/vues/conversion.jsp").forward(req,  resp);
	
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		double quantite = Integer.parseInt(req.getParameter("quantite"));
		String unite = req.getParameter("unite");
		
		if (quantite <= 0) {
			req.setAttribute("message", "valeur incorrect");
			doGet(req, resp);
		} else {
			// conversion en cours
		}
		
	
	}

}
