<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Lien CSS-->
    <link rel="stylesheet" href="css_files/Accueil.css">
    <link rel="stylesheet" href="css_files/footer.css">
    <link rel="stylesheet" href="css_files/index.css">
    <!-- ICONE et TITRE DE L'ONGLET -->
    <title>Accueil</title>
    <link rel="shortcut icon" href="A METTRE">
</head>


    <header role="header">
        <nav class="menu" role="navigation">
            <div class="inner"> 
                <div class="m-left">
                    <h1 class="logo">CUISINE MOI</h1>
                </div>
                <div class="m-right">
                    <a href="index.jsp" class="m-link">Accueil</a>
                    <a href="chrono.jsp" class="m-link">Chrono</a>
                    <a href="decongelation.jsp" class="m-link">Décongélation</a>
                    <a href="conversion" class="m-link">Conversion</a>
                </div>
            </div>
        </nav>
    </header>
<body>
